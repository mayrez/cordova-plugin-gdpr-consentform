cordova-plugin-gdpr-consentform
===============================

Cordova plugin to show the admob GDPR consent form and retrieve the user values for Android apps.

# Installation

This plugin follows the Cordova plugin spec, so it can be installed through the Cordova CLI in your existing Cordova project:

```
cordova plugin add https://mayrez@bitbucket.org/mayrez/cordova-plugin-gdpr-consentform.git
```

# How to use it

```
cordova.plugins.gdpr.showForm('pub-xxxxxxxxxxxxxx', 'https://yourprivacypolicyurl.com')
```

