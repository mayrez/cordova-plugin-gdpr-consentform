var exec = require('cordova/exec');

exports.showForm = function (pubId, privacyPolicyUrl) {
    return new Promise((resolve, reject) =>  {
        exec(resolve, reject, 'gdpr', 'showForm', [pubId, privacyPolicyUrl]);
    })
};
