package gdpr;

import org.apache.cordova.*;
import android.app.Activity;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import android.content.Context;
import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.ads.consent.*;

import java.net.MalformedURLException;
import java.net.URL;

public class gdpr extends CordovaPlugin {
    private ConsentForm form;
    private String pubId;
    private String privacyPolicyUrl;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("showForm")) {
            if(args.length() == 2){
                pubId = args.getString(0);
                privacyPolicyUrl = args.getString(1);
                this.showForm(callbackContext);
                return true;
            } 
        }

        return false;
    }

    protected void showConsentForm(){
        form.show();
    }

    private void showForm(CallbackContext callbackContext) {
        Context context = this.cordova.getActivity().getApplicationContext();
        Activity activity = this.cordova.getActivity();
        ConsentInformation consentInformation = ConsentInformation.getInstance(context);
        final String pub = pubId;
        String[] publisherIds = {pub};

        consentInformation.requestConsentInfoUpdate(publisherIds, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                switch (consentStatus){
                    case PERSONALIZED:
                        callbackContext.success("{\"statusCode\": 1, \"response\": " + true+ "}");           
                        break;
                    case NON_PERSONALIZED:
                        callbackContext.success("{\"statusCode\": 1, \"response\": " + false+ "}");
                        break;
                    case UNKNOWN:
                        if(ConsentInformation.getInstance(activity).isRequestLocationInEeaOrUnknown()){
                            URL privacyUrl = null;
                            try {
                                privacyUrl = new URL(privacyPolicyUrl);
                            } catch (MalformedURLException e) {
                                e.printStackTrace();

                            }

                            form = new ConsentForm.Builder(activity, privacyUrl)
                                    .withListener(new ConsentFormListener() {
                                        @Override
                                        public void onConsentFormLoaded() {
                                            showConsentForm();
                                        }

                                        @Override
                                        public void onConsentFormOpened() {}

                                        @Override
                                        public void onConsentFormClosed(ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                                            callbackContext.success("{\"statusCode\": 1, \"response\": " + userPrefersAdFree+ "}");
                                        }

                                        @Override
                                        public void onConsentFormError(String error) {
                                            callbackContext.success("{\"statusCode\": -1, \"response\": " + error + "}");
                                        }
                                    })
                                    .withPersonalizedAdsOption()
                                    .withNonPersonalizedAdsOption()
                                    .build();
                            form.load();
                        }

                    break;
                }

            }

            @Override
            public void onFailedToUpdateConsentInfo(String error){
                callbackContext.error("{\"statusCode\": -1, \"response\": \"GDPR consent form error: "+error+"\"}");
            }
        });
    }
}